# WorldPay
WorldPay 支付

## 安装
```
composer require jinyin/worldpay
```

## 配置
需要 WorldPay 相关参数，具体见下代码

## 使用
```
use Jinyin\WorldPay\WorldPay;

// 是否为沙箱环境 [true、false]
$sandbox = true;  
// 商家id
$installationId = xxxxx;
// 商家代码 通常为大写
$merchantCode = 'xxxxxxxx';
// 描述
$description = 'world pay for test xxxxx';
// 货币代码
$currencyCode = 'JPY';
// 金额保留小数位
$exponent = 0;
// 收货/账单联系人的 ISO 国家代码
$countryCode = 'JP';
// 用户名
$key = 'xxxxxxxx';
// 密码
$value = 'xxxxxxxxx';

$worldPay = new WorldPay($sandbox, $installationId, $merchantCode, $description, $currencyCode, $exponent, $countryCode, $key, $value);
```

## 获取支付链接
```
$order = [
    'order_sn' => 'test-' . rand(1000, 10000),
    'final_price' => 100,
    'email' => 'chenjianeng@inkanshop.com',
    'address' => '千葉県船橋市高根台2-9-2',
    'zip_code' => '2740065',
    'country' => '日本',
];
$result = $worldPay->getPayUrl($order);
```

## 在 Laravel 中使用
在 Laravel 中使用也是同样的安装方式，配置写在 `config/services.php` 中
```
·
·
·
'worldpay' => [
    'sandbox' => env('WORLDPAY_SANDBOX', 'true'),
    'installation_id' => env('WORLDPAY_INSTALLATION_ID', ''),
    'merchant_code' => env('WORLDPAY_MERCHANT_CODE', ''),
    'description' => env('WORLDPAY_DESCRIPTION', ''),
    'currency_code' => env('WORLDPAY_CURRENCY_CODE', ''), 
    'exponent' =>  env('WORLDPAY_EXPONENT', ''),
    'country_code' =>  env('WORLDPAY_COUNTRY_CODE', ''),
    'username' => env('WORLDPAY_USERNAME', ''),
    'password' => env('WORLDPAY_PASSWORD', ''),
]
```

然后在 `.env` 中配置以上参数
```
WORLDPAY_SANDBOX=
WORLDPAY_INSTALLATION_ID=
WORLDPAY_MERCHANT_CODE=
WORLDPAY_DESCRIPTION=
WORLDPAY_CURRENCY_CODE=
WORLDPAY_EXPONENT=
WORLDPAY_COUNTRY_CODE=
WORLDPAY_USERNAME=
WORLDPAY_PASSWORD=
```

### 方法参数注入
```
public function test(WorldPay $worldPay)
{
    $order = [
        'order_sn' => 'test-' . rand(1000, 10000),
        'final_price' => 100,
        'email' => 'chenjianeng@inkanshop.com',
        'address' => '千葉県船橋市高根台2-9-2',
        'zip_code' => '2740065',
        'country' => '日本',
    ];
    $result = $worldPay->getPayUrl($order);
}
```

### 服务器访问
```
public function test()
{
    $order = [
        'order_sn' => 'test-' . rand(1000, 10000),
        'final_price' => 100,
        'email' => 'chenjianeng@inkanshop.com',
        'address' => '千葉県船橋市高根台2-9-2',
        'zip_code' => '2740065',
        'country' => '日本',
    ];
    $result = app('worldpay')->getPayUrl($order);
}
```

## 参考
- [官方文档](http://developer-zh.worldpay.com/support/kb/gg/corporate-gateway-guide/content/hostedintegration/quickstart.htm)

## 前台页面 JavaScript SDK 参考代码
```
<html>
<head>
    <meta http-equiv="content-type" content="txt/html; charset=utf-8"/>
    <!-- 最新版本的 Bootstrap 核心 CSS 文件 -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <script src="https://apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js">
    </script>
    <!-- 最新的 Bootstrap 核心 JavaScript 文件 -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous">

    </script>
    <link rel='stylesheet'
          href='https://payments.worldpay.com/resources/hpp/integrations/embedded/css/hpp-embedded-integration-library.css'/>
    <script src='https://payments.worldpay.com/resources/hpp/integrations/embedded/js/hpp-embedded-integration-library.js'></script>
    <style>
        #wp-cl-custom-html-iframe {
            height: 680px !important;
        }

        form {
            padding-left: 15px;
        }

        p {
            color: red;
        }

        .t {
            margin: 0;
        }

        .form-group {
            margin-bottom: 0;
        }

        .form-control {
            width: 40%;
        }
    </style>
</head>
<body>
<hr style="margin-top: 0;">
<form id="form">
    <div class="form-group">
        <p>请求</p>
    </div>
    <div class="form-group">
        <input type="text" name="order[order_sn]" class="form-control" value="test12345678" placeholder="请输入订单号">
        <p class="t">示例: test12345678</p><br/>
    </div>
    <div class="form-group">
        <input type="text" name="order[final_price]" class="form-control" value="100" placeholder="请输入订单金额">
        <p class="t">示例: 100</p><br/>
    </div>
    <div class="form-group">
        <input type="text" name="description" class="form-control" value="world pay test" placeholder="请输入订单描述">
        <p class="t">示例: world pay test</p><br/>
    </div>
    <div class="form-group">
        <input type="text" name="order[email]" class="form-control" value="chenjianeng@inkanshop.com"
               placeholder="请输入邮箱">
        <p class="t">示例: chenjianeng@inkanshop.com</p><br/>
    </div>
    <div class="form-group">
        <input type="text" name="order[zip_code]" class="form-control" value="2740065" placeholder="请输入邮编">
        <p class="t">示例: 2740065</p><br/>
    </div>
    <div class="form-group">
        <input type="text" name="order[country]" class="form-control" value="日本" placeholder="请输入国家">
        <p class="t">示例: 日本</p><br/>
    </div>
    <div class="form-group">
        <input type="text" name="order[address]" class="form-control" value="千葉県船橋市高根台2-9-2" placeholder="请输入地址">
        <p class="t">示例: 千葉県船橋市高根台2-9-2</p><br/>
    </div>
    <button type="button" class="btn btn-default" id="submit">提交</button>

    <div id='custom-html'></div>
    <button id="custom-trigger" type="button" class="btn btn-default" style="display: none">test click</button>
</form>
<script>
    $('#submit').on('click', function () {
        var host = window.location.origin
        $.ajax({
            type: 'post',
            url: 'worldpay.php',
            data: $('#form').serialize(),
            dataType: 'json',
            success: function (result) {
                if (result.reply.hasOwnProperty('orderStatus')) {
                    worldpay(host, result.reply.orderStatus.reference)
                    $('#custom-trigger').click()
                }
                if (result.reply.hasOwnProperty('error')) {
                    alert(result.reply.error);
                }
            },
            error: function (result) {
                console.log(result)
            }
        })
    })

    function worldpay(host, url) {
        //your options
        var customOptions = {
            iframeIntegrationId: 'libraryObject',
            iframeHelperURL: host + '/worldpay/helper.html',
            iframeBaseURL: host + '/worldpay/worldpay.html',
            url: url,
            type: 'lightbox',
            trigger: 'custom-trigger',
            lightboxMaskOpacity: 100,
            lightboxMaskColor: '#ffffff',
            lightboxShadowColor: '#666',
            lightboxShadowSize: '30px',
            accessibility: true,
            target: 'custom-html',
            debug: false,
            language: 'ja',
            country: 'JP',
            preferredPaymentMethod: '',
            successURL: host + '/worldpay/success.html',
            cancelURL: host + '/worldpay/cancel.html',
            failureURL: host + '/worldpay/failure.html',
            pendingURL: host + '/worldpay/pending.html',
            errorURL: host + '/worldpay/error.html'
        };

        //initialise the library and pass options
        var libraryObject = new WPCL.Library();
        libraryObject.setup(customOptions);
    }
</script>
</body>
</html>
```
