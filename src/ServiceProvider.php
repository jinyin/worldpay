<?php

namespace Jinyin\WorldPay;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    protected $defer = true;

    public function register()
    {
        $this->app->singleton(WorldPay::class, function () {
            return new WorldPay(
                config('services.worldpay.sandbox'),
                config('services.worldpay.installation_id'),
                config('services.worldpay.merchant_code'),
                config('services.worldpay.description'),
                config('services.worldpay.currency_code'),
                config('services.worldpay.exponent'),
                config('services.worldpay.country_code'),
                config('services.worldpay.username'),
                config('services.worldpay.password')
            );
        });

        $this->app->alias(WorldPay::class, 'worldpay');
    }

    public function provides()
    {
        return [WorldPay::class, 'worldpay'];
    }
}