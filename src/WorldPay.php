<?php

namespace Jinyin\WorldPay;

use GuzzleHttp\Client;
use Jinyin\WorldPay\Exceptions\HttpException;

class WorldPay
{
    protected $sandbox; // 是否为沙箱环境
    protected $installationId; // 商家 id
    protected $merchantCode; // 商家代码
    protected $description; // 描述
    protected $currencyCode; // 货币代码
    protected $exponent; // 小数位
    protected $countryCode; // 收货/账单联系人的 ISO 国家代码
    protected $username; // 账号
    protected $password; // 密码

    public function __construct($sandbox, $installationId, $merchantCode, $description, $currencyCode, $exponent, $countryCode, $username, $password)
    {
        $this->sandbox = $sandbox;
        $this->installationId = $installationId;
        $this->merchantCode = $merchantCode;
        $this->description = $description;
        $this->currencyCode = $currencyCode;
        $this->exponent = $exponent;
        $this->countryCode = $countryCode;
        $this->username = $username;
        $this->password = $password;
    }

    /**
     * 获取支付链接
     *
     * @param array $order
     * @return mixed
     * @throws HttpException
     */
    public function getPayUrl(array $order)
    {
        if (true === $this->sandbox) {
            $url = 'https://secure-test.worldpay.com/jsp/merchant/xml/paymentService.jsp';
        } else {
            $url = 'https://secure.worldpay.com/jsp/merchant/xml/paymentService.jsp';
        }

        $header = $this->getHeader();
        $xml = $this->getXml($order);
        $options = array();
        $options['headers'] = $header;
        $options['body'] = $xml;
        try {
            $client = new Client();
            $response = $client->request('POST', $url, $options);
            $result = json_decode(json_encode(simplexml_load_string($response->getBody()->getContents(), 'SimpleXMLElement', LIBXML_NOCDATA)), true);
            return $result;
        } catch (\Exception $exception) {
            throw new HttpException($exception->getMessage(), $exception->getCode(), $exception);
        }
    }

    /**
     * 获取请求头
     *
     * @return array
     */
    protected function getHeader()
    {
        $header = array(
            'Content-Type' => 'application/xml; charset=UTF-8', // 指定 xml 格式
            'Authorization' => ' Basic ' . base64_encode($this->username . ':' . $this->password) // 对账号密码进行 base64 加密
        );
        return $header;
    }

    /**
     * 获取 xml 格式数据
     *
     * @param $order
     * @return string
     */
    protected function getXml($order)
    {
        $xml = '<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE paymentService PUBLIC "-//Worldpay//DTD Worldpay PaymentService v1//EN" 
  "http://dtd.worldpay.com/paymentService_v1.dtd">
<paymentService version="1.4" merchantCode="' . $this->merchantCode . '"> <!--Enter your own merchant code-->
  <submit>
    <order orderCode="' . $order['order_sn'] . '" installationId="' . $this->installationId . '"> <!--Enter a unique order code each time-->
      <description>' . $this->description . '</description> <!--Enter a description useful to you-->
      <amount currencyCode="' . $this->currencyCode . '" exponent="' . $this->exponent . '" value="' . $order['final_price'] . '"/>
      <paymentMethodMask>
        <include code="ALL"/>
      </paymentMethodMask>
      <shopper>
        <shopperEmailAddress>' . $order['email'] . '</shopperEmailAddress>
      </shopper>
      <billingAddress>
        <address> 
          <address1>' . $order['address'] . '</address1>
          <postalCode>' . $order['zip_code'] . '</postalCode>
          <city>' . $order['country'] . '</city>
          <countryCode>' . $this->countryCode . '</countryCode>
        </address>
      </billingAddress>
    </order>
  </submit>
</paymentService>';
        return $xml;
    }

}